package com.adobe.ags.migrator;

import com.adobe.ags.migrator.dam.DamRunnable;
import com.adobe.ags.migrator.parser.ContentMigration;
import com.adobe.ags.migrator.utils.AddCDATA;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.jackrabbit.commons.JcrUtils;

import javax.jcr.Credentials;
import javax.jcr.Repository;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import java.io.File;

public class Migrator extends Application {

    private ProgressBar pb = new ProgressBar(0);
    private Button startButton = new Button("Import");
    private File sourceDirectory = null;
    private static File mappingFile = null;
    private static String user = null;
	private static String pass = null;
	private static String server = null;
	private static String sourceDir = null;
	private static String cqDestDir = null;
	private static String migrationTimeout = null;
	private static boolean migrateAssets = false;
	private static boolean addCdata = false;
    private Session session = null;
    private TextField host;
    TextField username;
    PasswordField password;
    
    private TextField destDir;
    private TextField timeout;
    private TextField imgExt;
    private TextField refAssetExt;

    private Pane splashLayout;
    private Pane aboutLayout;
    private static final int SPLASH_WIDTH = 676;
    private static final int SPLASH_HEIGHT = 227;
    private Stage mainStage;
    private Stage aboutStage;
    TextArea debugWindow = new TextArea();
    private int sourceSystem = 0;

    private static final int AEM_56x = 0;
    private static final int OTHER = 1;
    
    public static final String CONST_RESOURCES ="/resources/";
   // public static final String CONST_RESOURCES ="/";
    
    
    public static String getUser() {
		return user;
	}

	public static void setUser(String user) {
		Migrator.user = user;
	}

	public static boolean isMigrateAssets() {
		return migrateAssets;
	}

	public static void setMigrateAssets(boolean migrateAssets) {
		Migrator.migrateAssets = migrateAssets;
	}


	public static boolean isAddCdata() {
		return addCdata;
	}

	public static void setAddCdata(boolean addCdata) {
		Migrator.addCdata = addCdata;
	}

	public static String getSourceDir() {
		return sourceDir;
	}

	public static void setSourceDir(String sourceDir) {
		Migrator.sourceDir = sourceDir;
	}


	public static String getCqDestDir() {
		return cqDestDir;
	}

	public static void setCqDestDir(String cqDestDir) {
		Migrator.cqDestDir = cqDestDir;
	}

	public static String getMigrationTimeout() {
		return migrationTimeout;
	}

	public static void setMigrationTimeout(String migrationTimeout) {
		Migrator.migrationTimeout = migrationTimeout;
	}

	public static String getServer() {
		return server;
	}

	public static void setServer(String server) {
		Migrator.server = server;
	}

	public static String getPass() {
		return pass;
	}

	public static void setPass(String pass) {
		Migrator.pass = pass;
	}

    public static File getConfigurationFile() {
		return mappingFile;
	}

	public void setConfigurationFile(File mappingFile) {
		Migrator.mappingFile = mappingFile;
	}

	public static void main(String [] args) {
    	try
    	{
    		launch(args);
    	}catch(Exception e)
    	{
    		System.out.println("Exception:"+e);
    	}
        
    }

    public void start(Stage primaryStage) {
        aboutStage = new Stage(StageStyle.TRANSPARENT);

        initAbout();
        showMainStage();

        primaryStage.toFront();
    }

    private void showMainStage() {
        mainStage = new Stage(StageStyle.DECORATED);
        MenuBar menuBar = new MenuBar();
        Menu fileMenu = new Menu("File");
        MenuItem fileQuit = new MenuItem("Quit Migrator");
        MenuItem fileSettings = new MenuItem("Settings");
        fileMenu.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent actionEvent) {
                System.exit(0);
                session.logout();
            }
        });
        fileMenu.getItems().addAll(fileSettings, new SeparatorMenuItem(), fileQuit);
        Menu editMenu = new Menu("Edit");
        Menu helpMenu = new Menu("Help");
        MenuItem aboutMenu = new MenuItem("About ACS Migrator");
        aboutMenu.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent actionEvent) {
                showAbout();
            }
        });
                helpMenu.getItems().addAll(aboutMenu);
        menuBar.getMenus().addAll(fileMenu,editMenu,helpMenu);

        mainStage.setTitle("ACS Migrator");
        mainStage.setResizable(false);
        mainStage.setMinHeight(600);
        mainStage.setMinWidth(800);

        BorderPane root = new BorderPane();


        //root.getChildren().addAll(menuBar);
        //((VBox) scene.getRoot()).getChildren().addAll(menuBar);
        root.setTop(menuBar);
        root.setCenter(getCenter());
        root.setLeft(getLeft());
        Scene scene = new Scene(root, 300, 250);
        mainStage.setScene(scene);
        mainStage.show();
    }

    @SuppressWarnings("rawtypes")
	private HBox getLeft() {
        HBox hbox = new HBox();

        VBox vbox = new VBox(5);
        vbox.setPadding(new Insets(10, 10, 10, 10));
        vbox.setAlignment(Pos.TOP_LEFT);
        vbox.setFillWidth(true);

        final ChoiceBox source = new ChoiceBox(FXCollections.observableArrayList("Source System", "Images","Documentum XML","Add CDATA"));
        source.getSelectionModel().selectFirst();
        source.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number new_value) {
                sourceSystem = new_value.intValue();
                switch (new_value.intValue()) {
                    case 0: break;
                    case 1: break;
     
                    default: break;
                }
                addDebugMessage(source.getItems().get(new_value.intValue()).toString());
            }
        });
        final ChoiceBox type = new ChoiceBox(FXCollections.observableArrayList("AEM 5.6.x","CQ 5.5"));
        type.getSelectionModel().selectFirst();
        type.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number new_value) {
                sourceSystem = new_value.intValue();
                addDebugMessage(type.getItems().get(new_value.intValue()).toString());
            }
        });
        
        
        
        destDir = new TextField("/content/sdc/parks");
        destDir.setPrefWidth(200);
        destDir.setMinWidth(200);

        timeout = new TextField("3");
        timeout.setPrefWidth(40);
        timeout.setMinWidth(40);
        
        host = new TextField("http://localhost:4502");
        host.setPrefWidth(200);
        host.setMinWidth(200);
        username = new TextField("admin");
        password = new PasswordField();
        password.setText("admin");
        password.setPromptText("Password");
        
        
      

        Button btn = new Button();
        btn.setText("Source Directory...");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent actionEvent) {
                DirectoryChooser chooser = new DirectoryChooser();
                sourceDirectory = chooser.showDialog(null);
                if (sourceDirectory != null) {
                	setSourceDir(sourceDirectory.getAbsolutePath());
                    addDebugMessage(sourceDirectory.getAbsolutePath());
                }
            }
        });


        
        Button mapping_button = new Button();
        mapping_button.setText("Migration mapping file...");
        mapping_button.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent actionEvent) {
            	FileChooser chooser = new FileChooser();
            	mappingFile = chooser.showOpenDialog(null);
                if (mappingFile != null) {
                	setConfigurationFile(mappingFile);
                }
            }
        });
     

        CheckBox recursive = new CheckBox("Migrate Assets");
        recursive.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent actionEvent) {
            	setMigrateAssets(true);
            }
        });
        
        CheckBox cdata = new CheckBox("Add CDATA");
        cdata.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent actionEvent) {
            	setAddCdata(true);
            }
        });


        vbox.getChildren().addAll(source, type,new Text("URL"), host, new Text("Username"), username, new Text("Password"), password,new Text("CQ desitnation directory"), destDir,btn,mapping_button,cdata, recursive,new Text("Asset migration timeout"), timeout);
        hbox.getChildren().addAll(new Separator(Orientation.VERTICAL), vbox);

        return hbox;
    }

    private HBox getCenter() {
        HBox hbox = new HBox();

        VBox vbox = new VBox(5);
        vbox.setPadding(new Insets(10, 10, 10, 10));
        vbox.setAlignment(Pos.TOP_LEFT);

        pb.setDisable(true);
        startButton.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent actionEvent) {
                addDebugMessage("button pressed");

                switch (sourceSystem) {
                case 1: runDAMMigration();
                		break;
                case 2: runContentMigration();
                		break;
                case 3: runCDATA();
        				break;
                default: break;
            }
                
                System.out.println("Source System :"+sourceSystem);
             
            }
        });
        //debugWindow = new TextArea();
        debugWindow.setPrefColumnCount(55);
        debugWindow.setPrefRowCount(47);
        vbox.getChildren().addAll(pb, startButton,debugWindow);
        hbox.getChildren().addAll(new Separator(Orientation.VERTICAL), vbox);

        return hbox;
    }
    
    private void runCDATA(){

        boolean startThread = true;
        boolean cdataThread = true;
        
        setUser(username.getText());
        setPass(password.getText());
        setCqDestDir(destDir.getText());
        setServer(host.getText());

        if (cdataThread) {
            startButton.setDisable(true);
            Thread process = new Thread(new AddCDATA(sourceDirectory, session, pb, startButton));
            process.start();
        }
        
        if (startThread) {
            startButton.setDisable(true);
            Thread process = new Thread(new ContentMigration(sourceDirectory, session, pb, startButton));
            process.start();
        }
}
    
    private void runContentMigration(){

            boolean startThread = true;
            
            setUser(username.getText());
            setPass(password.getText());
            setCqDestDir(destDir.getText());
            setServer(host.getText());
            setMigrationTimeout(timeout.getText());

            if (startThread) {
                startButton.setDisable(true);
                Thread process = new Thread(new ContentMigration(sourceDirectory, session, pb, startButton));
                process.start();
            }
 }
    
    private void runDAMMigration(){
    	   if (isValidConfiguration()) {

               boolean startThread = false;

               try {
                   Repository repo = JcrUtils.getRepository(host.getText());
                   Credentials credentials = new SimpleCredentials("admin", "admin".toCharArray());
                   session = repo.login(credentials);

                   if (session.isLive()) {
                       addDebugMessage("JCR Connection valid.");
                       startThread = true;
                   }
               } catch (Exception e) {
                   startThread = false;
                   System.out.println(e.getMessage());
               }

               if (startThread) {
                   startButton.setDisable(true);
                   Thread process = new Thread(new DamRunnable(sourceDirectory, session, pb, startButton));
                   process.start();
               }
           }
    }

    private boolean isValidConfiguration() {

        boolean rval = false;

        if (sourceDirectory != null && sourceDirectory.getAbsoluteFile().isDirectory()) {
            addDebugMessage("directory is OK");
            rval = true;
            if (sourceSystem == 0) {
                addDebugMessage("AEM is the targeted system");
            }
        }
        return rval;
    }

    public void init() {
        ImageView splash = new ImageView(new Image(Migrator.CONST_RESOURCES+"images/migrator-splash.png"));
        splashLayout = new VBox();
        splashLayout.getChildren().addAll(splash);
        splashLayout.setStyle("-fx-padding: 5; -fx-background-color: white; -fx-border-width:1; -fx-border-color: linear-gradient(to bottom, chocolate, derive(chocolate, 50%));");
        splashLayout.setEffect(new DropShadow());

        aboutLayout = new VBox();
        aboutLayout.getChildren().addAll(splash);
        aboutLayout.setStyle("-fx-padding: 5; -fx-background-color: white; -fx-border-width:1; -fx-border-color: linear-gradient(to bottom, chocolate, derive(chocolate, 50%));");
        aboutLayout.setEffect(new DropShadow());
    }

    /*
    private void showSplash(final Stage initStage) {
        Scene splashScene = new Scene(splashLayout);
        splashScene.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                initStage.hide();
            }
        });
        initStage.initStyle(StageStyle.UNDECORATED);
        final Rectangle2D bounds = Screen.getPrimary().getBounds();
        initStage.setScene(splashScene);
        initStage.setX(bounds.getMinX() + bounds.getWidth() / 2 - SPLASH_WIDTH / 2);
        initStage.setY(bounds.getMinY() + bounds.getHeight() / 2 - SPLASH_HEIGHT / 2);
        initStage.show();
    }
    */

    private void showAbout() {
        aboutStage.show();
    }

    private void initAbout() {
        Scene aboutScene = new Scene(aboutLayout);
        aboutScene.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                aboutStage.hide();
            }
        });
        aboutStage.initStyle(StageStyle.UNDECORATED);
        final Rectangle2D bounds = Screen.getPrimary().getBounds();
        aboutStage.setScene(aboutScene);
        aboutStage.setX(bounds.getMinX() + bounds.getWidth() / 2 - SPLASH_WIDTH / 2);
        aboutStage.setY(bounds.getMinY() + bounds.getHeight() / 2 - SPLASH_HEIGHT / 2);
    }

    private void addDebugMessage(String message) {
        debugWindow.appendText(message + "\n");
    }

}