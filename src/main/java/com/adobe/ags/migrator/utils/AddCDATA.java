package com.adobe.ags.migrator.utils;

import java.io.File;
import java.util.Map;

import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;

import javax.jcr.Session;

import com.adobe.ags.migrator.Migrator;
import com.adobe.ags.migrator.parser.ContentMigration;

public class AddCDATA implements Runnable {
	
	public  AddCDATA(File sourceDirectory, Session session, ProgressBar pb,
			Button startButton) {
	  	String mapping = Migrator.getConfigurationFile().getAbsolutePath();
    	new ContentMigration(mapping);
    	
	MigrationUtil.addCdata(sourceDirectory.getAbsolutePath(), ContentMigration.getMigrationMap());
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}

}
