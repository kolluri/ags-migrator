package com.adobe.ags.migrator.utils;

import java.text.ParseException;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.apache.jackrabbit.util.ISO8601;

/**
 * Date Util class
 * 
 * @author gkolluri
 * 
 */
public class MigrationDateUtil {

	final static String ISO_8601_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

	/**
	 * This method accepts date in source format and returns standard ISO 8601
	 * date used by CQ in string format
	 * 
	 * @param date
	 * @param sourceFormat
	 * @return
	 * @throws ParseException
	 */
	public static String getISO8601Date(String date, String sourceFormat) {
		String dateInISO8601 = "";
		if (date != null) {
			dateInISO8601 = ISO8601.format(getCalenderDate(date, sourceFormat));
		}

		return dateInISO8601;
	}

	public static Calendar getCalenderDate(String date, String sourceFormat) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat(sourceFormat);
		Date standardDate = new Date();
		try {
			if (date != null) {
				standardDate = formatter.parse(date);
			}
		} catch (ParseException e) {
			System.out.print("Error While Parsing date:" + e.getMessage());
		}
		cal.setTime(standardDate);
		return cal;
	}

	public static String getPublishedYear(String date, String format) {
		Calendar cal = getCalenderDate(date, format);
		return Integer.toString(cal.get(Calendar.YEAR));
	}

	public static String getPublishedMonth(String date, String format) {
		Calendar cal = getCalenderDate(date, format);
		DateFormatSymbols dg = DateFormatSymbols.getInstance();
		return dg.getShortMonths()[cal.get(Calendar.MONTH)];
	}

}
