package com.adobe.ags.migrator.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.client.ClientProtocolException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.adobe.ags.migrator.Migrator;
import com.day.cq.commons.jcr.JcrUtil;

public class MigrateReferencesUtil {
	

	public static String getMigratedReferences(String text, String fileName, String assetPath, BufferedWriter errorWriter){

        	try {
        		if(assetPath.contains("?")){
        			assetPath = assetPath.substring(0,assetPath.lastIndexOf("?"));
        		}
        		
				String migratedPath = getMigratedPath(assetPath,fileName, errorWriter);
				
				if(migratedPath!=null){
					text = text.replaceAll(assetPath,  migratedPath);
				}
        	}
        	 catch (Exception e) {
					System.out.println("Excepting while migrating page references :" +assetPath);
					
				}
        
        return text;
		
	}

	public static String getMigratedPath(String referencePath, String fileName, BufferedWriter errorWriter) {
	
		String cqPath = null;
		String url = null;
		String suffix = null;
		fileName = fileName.replace(File.separator, "/");
		try {
			System.out.println("Reference path is "+ referencePath);
			System.out.println("File pwd is "+fileName);
			String department = Migrator.getCqDestDir();
			department = department.substring(department.lastIndexOf("/"));
			if(referencePath.startsWith("#")){
				return null;
			}
			if(referencePath.startsWith("../")){
				if(referencePath.contains(department)){
					referencePath = referencePath.replace(department, "");
				}
				url = "http://www.sdcounty.ca.gov"+department+referencePath.substring(2);
			}else if(referencePath.startsWith("http://www.sdcounty.ca.gov") || referencePath.startsWith("http://www.co.san-diego.ca.us")){
				url = referencePath;
			}else if(referencePath.startsWith("/")){
				 url = "http://www.sdcounty.ca.gov"+referencePath;
			}else{
				if(referencePath.startsWith("http")){
					return referencePath;
				}else{
					 department = Migrator.getCqDestDir();
					department = department.substring(department.lastIndexOf("/"));
					System.out.println("PWD for file is "+fileName.substring(0,fileName.lastIndexOf("/")));
					url = "http://www.sdcounty.ca.gov"+department+fileName.substring(0,fileName.lastIndexOf("/"))+"/"+referencePath;
					System.out.println("SDC path is "+url);
				}
				
			}
			
			if(url.contains("#")){
				suffix = url.substring(url.lastIndexOf("#"),url.length());
				url = url.substring(0,url.lastIndexOf("#"));
				
				System.out.println("Truncated url is "+url);
				System.out.println("Suffix of the url is "+suffix);
			}
			
			URL urli = new URL(url);
			String path = urli.getPath();
			System.out.println("Reference path for migration :"+referencePath);
			if(referencePath.endsWith("index.html") || referencePath.endsWith("/")){
				cqPath = "/content/sdc"+urli.getPath().substring(0, urli.getPath().lastIndexOf("/"))+".html";
				System.out.println("Migrated reference path for default page :"+cqPath);
			}else{
				cqPath = "/content/sdc"+path;
				System.out.println("Migrated reference path :"+cqPath);
			}
			
			if(suffix != null){
				cqPath = cqPath + suffix;
				System.out.println("Migrated reference path with suffix is:"+cqPath);
			}
			
		} catch (IOException e) {
			System.out.println("Exception while retiving source page title: Page path - "+url);
			try {
				errorWriter.write("\n [ERROR] Exception while migrating page reference  "+url);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		
		return cqPath;
	}
	

}
