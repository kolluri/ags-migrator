package com.adobe.ags.migrator.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.IOUtils;

import com.adobe.ags.migrator.Migrator;
import com.day.jcr.vault.fs.api.PathFilterSet;
import com.day.jcr.vault.fs.config.DefaultWorkspaceFilter;

/**
 * @author gkolluri Package class is used to created target content directories
 *         and metadata required for CRX package creation
 */
public class Package {
	static List<String> docs = new ArrayList<String>();
	private File cwd;
	private String contentDir;
	private String migrationDir;

	static final String DEST_DIRECTORY = "migration";
	static final String JCR_ROOT_DIRECOTY = DEST_DIRECTORY + "/jcr_root/";
	static final String VAULT_DIRECOTY = DEST_DIRECTORY + "/META-INF/vault";
	static final String OUTPUT_FILENAE = "ContentMigration.zip";

	/**
	 * Create destination content directory in migration/jcr_root folder
	 * 
	 * @param contentDir
	 */
	public Package(String contentDir) {
		cwd = new File(JCR_ROOT_DIRECOTY + contentDir);
		File migration = new File(DEST_DIRECTORY);
		migrationDir = migration.getAbsolutePath();
		if(!cwd.exists()){
			cwd.mkdirs();
			addContent(cwd.getPath().substring(18));
				File contentSubTypeDescriptor = new File(cwd, ".content.xml");
				Template t;
				try {
					t = new Template("sdcinternal.xml");
					t.setParameter("title", cwd.getName());
					t.setParameter("text", "");
					FileWriter out = new FileWriter(contentSubTypeDescriptor);
					t.write(out);
				OutputStreamWriter out1 = new OutputStreamWriter(new FileOutputStream(contentSubTypeDescriptor),"UTF8");
					t.write(out1);
					out1.close();
				}catch(Exception e){
					System.out.println("Expection while creating folder");
				}
		}
	
		this.contentDir = contentDir;
	}

	public String getContentBaseDir() {
		return contentDir;
	}

	public File mkdirs(String path) {
		File dir = new File(JCR_ROOT_DIRECOTY + path);
		dir.mkdirs();
		return dir;
	}

	/**
	 * This method created folders
	 * 
	 * @param path
	 * @param title
	 * @return
	 * @throws IOException
	 */
	public File createFolder(String path, String title, String template)
			throws IOException {
		if(path == null){
			return cwd;
		}
		File dir = new File(cwd, path);
		
			dir.mkdirs();

			if (title != null) {
				File contentSubTypeDescriptor = new File(dir, ".content.xml");
				Template t = new Template(template);
				t.setParameter("title", title);
			
				
				try{
					FileWriter out = new FileWriter(contentSubTypeDescriptor);
					t.write(out);
				}catch(Exception e){
					System.out.println("Expection while creating folder");
				}
				
				OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(contentSubTypeDescriptor),"UTF8");
					t.write(out);
					out.close();
			}

		return dir;
	}

	public void addContent(String path) {
		docs.add(path);
	}

	/**
	 * This method creates metadata for CRX package, adding filter entries based
	 * on migrated content and use ContentPackageUtil to archive jcr_root and
	 * META-INF folders into a zip file.
	 * 
	 * @throws IOException
	 */
	public void save(String packageName) throws IOException {
		File vaultDir = new File(VAULT_DIRECOTY);
		String[] files = { "config.xml", "nodetypes.cnd", "properties.xml" };
		vaultDir.mkdirs();
		for (String f : files) {
			try {InputStream is = getClass().getResourceAsStream(
				Migrator.CONST_RESOURCES+"migration/META-INF/vault/" + f); 
				FileOutputStream out = new FileOutputStream(new File(vaultDir,
						f));
				IOUtils.copy(is, out);
				out.close();
			}
			catch(Exception e){
				
			}
		}

		File f = new File(vaultDir, "filter.xml");
		DefaultWorkspaceFilter filter = new DefaultWorkspaceFilter();
		try{
			FileWriter fw = new FileWriter(f);
				for (String doc : docs) {
					filter.add(new PathFilterSet(doc.replace("\\","/")));
				}
				IOUtils.copy(filter.getSource(), fw);
				fw.close();
		}
		catch(Exception e){
			
		}

		final String OUTPUT_ZIP_FILE = migrationDir + File.separator + packageName+".zip";
		final String SOURCE_FOLDER = migrationDir;

		ContentPackageUtil.createContentPackage(SOURCE_FOLDER, OUTPUT_ZIP_FILE);

	}
}
