package com.adobe.ags.migrator.utils;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.text.StrSubstitutor;

import com.adobe.ags.migrator.Migrator;

import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.Map;
import java.util.HashMap;

/**
 * @author gkolluri This class is used to create target XML based on the content
 *         type template and set variable property values.
 */
public class Template {
	private String xml;
	private Map<String,String> valuesMap = new HashMap<String,String> ();

	public Template(String template) throws IOException {
		String templatePath = Migrator.CONST_RESOURCES+"template/" + template;
		InputStream is = getClass().getResourceAsStream(templatePath);
		xml = IOUtils.toString(is);
		is.close();
	}

	public void setParameter(String name, String value) {
		valuesMap.put(name, value == null ? "" : xmlEscape(value));
	}
	
	public void setXMLParameter(String name, String value) {
		valuesMap.put(name, value == null ? "" : value);
	}

	public void insertBefore(String text, String pattern) {
		int idx = xml.indexOf(pattern);
		if (idx == -1)
			throw new IllegalArgumentException("not found: " + pattern);

		xml = xml.substring(0, idx) + text + xml.substring(idx);
	}

	public void write(Writer out) throws IOException {
		StrSubstitutor sub = new StrSubstitutor(valuesMap);
		xml = sub.replace(xml);
		out.write(xml);
	}

	public String toString() {
		return xml;
	}

	/**
	 * Performs an XML escaping of a given string.
	 * 
	 */
	public static String xmlEscape(String value) {
		return StringEscapeUtils.escapeXml(value);
	}

}
