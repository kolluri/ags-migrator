package com.adobe.ags.migrator.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


/**
 * 
 * @author gkolluri This is a utility class used to archive migrated content
 *         into a zip file
 * 
 */
public class ContentPackageUtil {

	public static void createContentPackage(String source,String packageName  ) {

		List<String> fileList  = generateFileList(new File(source),
				source);
		createPackage(packageName, source, fileList);
	}
	
	
	/**
	 * Create a zip file from the migration directory
	 * 
	 * @param packageName
	 * @param source
	 * @param fileList 
	 */
	public static void createPackage(String packageName, String source, List<String> fileList) {
		byte[] buffer = new byte[1024];
		try {
			FileOutputStream fos = new FileOutputStream(packageName);
			ZipOutputStream zos = new ZipOutputStream(fos);
			for (String file : fileList) {
				System.out.println("File Added : " + file);
					ZipEntry ze = new ZipEntry(file);
					zos.putNextEntry(ze);
					FileInputStream in = new FileInputStream(source
							+ File.separator + file);
					int len;
					while ((len = in.read(buffer)) > 0) {
						zos.write(buffer, 0, len);
					}
					in.close();
			}
			zos.closeEntry();
			
			// remember close it
			zos.close();
			System.out.println("CRX Content package is created");
		} catch (IOException ex) {
			System.out.println("Error While creating content package");
		}
	}

	/**
	 * Traverse a directory and get all files, and add the file into fileList
	 * 
	 * @param node
	 *            file or directory
	 * @return 
	 */
	public static List<String> generateFileList(File node, String source) {

		List<String> fileList = new ArrayList<String>();
		// add file only
		if (node.isFile()) {
			fileList.add(generateZipEntry(node.getAbsoluteFile().toString(),
					source));
		}
		// add directory
		if (node.isDirectory()) {
			String[] subNote = node.list();
			for (String filename : subNote) {
				fileList.addAll(generateFileList(new File(node, filename), source));
				//generateFileList(new File(node, filename), source);
			}
		}
		
		return fileList;
	}

	/**
	 * Format the file path for content package
	 * 
	 * @param file
	 *            file path
	 * @return Formatted file path
	 */
	private static String generateZipEntry(String file, String source) {
		return file.substring(source.length() + 1, file.length());
	}

}