package com.adobe.ags.migrator.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.adobe.ags.migrator.Migrator;
import com.adobe.ags.migrator.parser.ContentMigration;

/**
 * This class is to provide utility methods for DAM migration
 * 
 * @author gkolluri
 * 
 */
public class MigrationUtil {
	
	
	
	public static void addCdata(String sourceDirectory, Map<String, String> migrationMap){
		

		
		File folder = new File(sourceDirectory);
		
		//Parsing through all the neurtal XML files in the source directory
		for (File fileEntry : folder.listFiles()) {
			if(fileEntry.isDirectory()){
				addCdata(fileEntry.getAbsolutePath(),migrationMap);
			}else if(fileEntry.getName().endsWith(".xml")){
				Path path = Paths.get(fileEntry.getAbsolutePath());
				System.out.println("Adding CDATA to file :"+path);
				Charset charset = StandardCharsets.UTF_8;
				
				String content;
				try {
					Map<String, String> map = migrationMap;
					content = new String(Files.readAllBytes(path), charset);
					if(migrationMap!=null && !content.contains("<![CDATA[")){
						  Iterator it = migrationMap.entrySet().iterator();
						    while (it.hasNext()) {
						        Map.Entry pairs = (Map.Entry)it.next();
						        if(!pairs.getKey().equals("migration_mapping")){
						        	content = content.replaceAll("<"+pairs.getKey()+">", "<"+pairs.getKey()+"><![CDATA[");
						        	content = content.replaceAll("</"+pairs.getKey()+">", "]]></"+pairs.getKey()+">");
						        }
						    }
					}
					Files.write(path, content.getBytes(charset));
				
					
				} catch (IOException e) {
					System.out.println("Exception while adding CDATA to source file. Path:"+path.toString());
				}
				
			}

		}
	}

	/**
	 * This method takes location of mapping file as input and reads the mapping
	 * file and return a DAM asset map
	 * 
	 * @param mappingFile
	 * @return
	 */
	public static Map<String, String> getMapping(String mappingFile) {
		Map<String, String> componentMap = new HashMap<String, String>();
		try {

			FileInputStream file = new FileInputStream(new File(mappingFile));

			// Get the workbook instance for XLS file
			XSSFWorkbook workbook = new XSSFWorkbook(file);

			// Get first sheet from the workbook
			XSSFSheet sheet = workbook.getSheetAt(0);

			// Iterate through each rows from first sheet
			Iterator<Row> rowIterator = sheet.iterator();


			if (rowIterator.hasNext()) {
				Row firstrow = rowIterator.next();
				if (firstrow.getRowNum() == 0) {
					//ignoring first row in spreedsheet
				}
			}

			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through each columns
				Iterator<Cell> cellIterator = row.cellIterator();
				// String fileName =cellname.getStringCellValue();
				String componentName = null;
				String componentPath = null;
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					switch (cell.getColumnIndex()) {
					case 0:
						componentName = cell.getStringCellValue().trim();
						break;
					case 1:
						componentPath = cell.getStringCellValue().trim();
						break;
					default:
						break;
					}
				}
				componentMap.put(componentName, componentPath);
			}
			file.close();
		} catch (FileNotFoundException fe) {
			System.out.println("Mapping file exception " + fe.getMessage());
		} catch (IOException e) {
			System.out
					.println("Input/Output exception while parsing mapping file "
							+ e.getMessage());
		}
		catch (Exception e) {
			System.out
					.println("Exception while parsing mapping file "
							+ e.getMessage());
		}
		return componentMap;
	}
}
