package com.adobe.ags.migrator.parser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;

import javax.jcr.Session;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.json.simple.JSONValue;
import org.jsoup.Jsoup;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.commons.jcr.JcrUtil;
import com.adobe.ags.migrator.Migrator;
import com.adobe.ags.migrator.utils.MigrateReferencesUtil;
import com.adobe.ags.migrator.utils.MigrationUtil;
import com.adobe.ags.migrator.utils.Package;
import com.adobe.ags.migrator.utils.Template;

public class ContentMigration extends DefaultHandler implements Runnable {
	String xmlFileName;
	String tmpValue;
	boolean isDescStarted = false;
	StringBuilder textDesc = new StringBuilder();
	static String contentDirec;
	static Package pkg;
	OutputStreamWriter out;
	File docDir;
	static File errofile;
	static BufferedWriter errorWriter;
	static File mappingfile;
	static BufferedWriter mappingWriter;
	static String fileName;
	private String currentFile;
	Boolean faq = false;
	Boolean isNavigation = false;
	Boolean isSublinks = false;
	static JSONObject jsonObj;
	static String faqQA;
	static TidyJSONWriter writer;
	static StringWriter sw;
	static int linksCounter = 0;
	static StringWriter templteWriter;
	

	public String getCurrentFile() {
		return currentFile;
	}

	public void setCurrentFile(String currentFile) {
		this.currentFile = currentFile;
	}

	static Map<String, String> migrationMap = new HashMap<String, String>();

	public static Map<String, String> getMigrationMap() {
		return migrationMap;
	}

	public static void setMigrationMap(Map<String, String> migrationMap) {
		ContentMigration.migrationMap = migrationMap;
	}

	private Map<String, String> valuesMap = new HashMap<String, String>();

	File sourceDirectory = null;
	Session session = null;
	ProgressBar progressBar = null;
	Button startButton = null;

	public ContentMigration(File directory, Session session, ProgressBar pb,
			Button startButton) {
		sourceDirectory = directory;
		this.session = session;
		progressBar = pb;
		this.startButton = startButton;
	}

	public ContentMigration(String xmlFileName) {

		this.xmlFileName = xmlFileName;
		pkg = new Package(contentDirec);
		parseDocument();
	}

	public ContentMigration(File currentFile, File sourceFolder) {

		String parentFolder = currentFile.getParentFile().getAbsolutePath();
		String newFolder = parentFolder.substring(sourceFolder
				.getAbsolutePath().length());
		this.xmlFileName = currentFile.getAbsolutePath();
		pkg = new Package(contentDirec + newFolder);
		parseDocument();
	}

	private void parseDocument() {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			SAXParser parser = factory.newSAXParser();
			System.out.println("\n Migrating file name" + xmlFileName + "\n ");
			parser.parse(xmlFileName, this);
		} catch (ParserConfigurationException e) {
			System.out.println("ParserConfig error");
		} catch (SAXException e) {
			System.out.println("SAXException : xml not well formed");
			try {
				errorWriter
						.write("\n [ERROR] SAXException : xml not well formed");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} catch (IOException e) {
			System.out.println("IO error");
		}
	}

	@Override
	public void startElement(String s, String s1, String elementName,
			Attributes attributes) throws SAXException {

		if (elementName.endsWith("text")
				&& !xmlFileName.endsWith("mapping.xml")) {
			isDescStarted = true;
		}

		if (elementName.equals("Left_Nav_Page")) {
			isNavigation = true;
			templteWriter = new StringWriter();
			linksCounter =0;
		}
		
		

		if (elementName.equals("faqs_page")) {
			faq = true;
			sw = new StringWriter();
			writer = new TidyJSONWriter(sw);
			writer.setTidy(true);
			try {
				writer.array();

			} catch (JSONException e) {
				System.out.println("Exception while migrating FAQ contents"
						+ e.getMessage());
			}
		}


		
		if (elementName.equals("Item")) {
			try {
				sw = new StringWriter();
				writer = new TidyJSONWriter(sw);
				writer.array();
			} catch (JSONException e) {
				System.out.println("Exception while migrating FAQ contents"
						+ e.getMessage());
			}

		}

		if (elementName.equals("questions_and_answers")) {
			try {
				writer.object();
			} catch (JSONException e) {
				System.out.println("Exception while migrating FAQ contents"
						+ e.getMessage());
			}

		}

	}

	@Override
	public void endElement(String s, String s1, String element)
			throws SAXException {

		if (xmlFileName.endsWith("mapping.xml")) {
			migrationMap.put(element, tmpValue);
			return;
		}

		if (element.equals("questions_and_answers")) {
			try {
				writer.endObject();
			} catch (JSONException e) {
				System.out.println("Exception while migrating FAQ contents"
						+ e.getMessage());
			}
		}

		if (element.equals("question")) {
			try {
				String test = StringEscapeUtils.escapeCsv(tmpValue);

				writer.key("question").value(
						StringEscapeUtils.escapeXml(tmpValue));
			} catch (JSONException e) {
				System.out.println("Exception while migrating FAQ contents"
						+ e.getMessage());
			}
			return;

		}

		if (element.equals("answer")) {

			try {
				String test = StringEscapeUtils.escapeCsv(tmpValue);
				writer.key("answer").value(test);
			} catch (JSONException e) {
				System.out.println("Exception while migrating FAQ contents"
						+ e.getMessage());
			}
			return;
		}



		if (element.equals("Sub_Label_Text")) {
			try {
				String subLinkText = StringEscapeUtils.escapeCsv(tmpValue);
				writer.object();
				if(!subLinkText.startsWith("\n")){
					writer.key("text").value(StringEscapeUtils.escapeXml(tmpValue));
				}else{
					writer.key("text").value("");
				}
				
			} catch (JSONException e) {
				System.out.println("Exception while migrating FAQ contents"
						+ e.getMessage());
			}
			return;

		}

		if (element.equals("Sub_Link_URL")) {

			try {
				String subNavLink = StringEscapeUtils.escapeCsv(tmpValue);
				String migratedLink = MigrateReferencesUtil.getMigratedPath(subNavLink, fileName, errorWriter);
				if(migratedLink != null){
					writer.key("link").value(migratedLink);
				}else{
					writer.key("link").value("");
				}
				
				writer.endObject();
			} catch (JSONException e) {
				System.out.println("Exception while migrating FAQ contents"
						+ e.getMessage());
			}
			return;
		}

		if (element.equals("Item")) {

			try {
				writer.endArray();
				
			} catch (JSONException e2) {
				System.out.println("Exception while migrating links");
			}

			StringBuffer stringBuffer = sw.getBuffer();
			try {
				sw.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			String sublinks = stringBuffer.toString().replace("\",\"","\"\\,\"");

			try {
				Template t = new Template("menulinks.xml");

				for (Map.Entry<String, String> entry : valuesMap.entrySet()) {
					t.setParameter(entry.getKey(), entry.getValue());
				}

				if (isNavigation) {
					t.setParameter("counter", Integer.toString(linksCounter++));
					if(isSublinks){
						t.setParameter("sublinks", sublinks);
					}else{
						t.setParameter("sublinks", "[]");
					}
					
				}

				t.write(templteWriter);

				System.out.println("Menu links  content "
						+ templteWriter.getBuffer().toString());

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return;
		}

		if (element.equals("dept_secondary_page")
				|| element.equals("press_release")
				|| element.equals("dept_primary")
				|| element.equals("faqs_page")
				|| element.equals("Left_Nav_Page")) {

			String templateFile = null;
			if (faq) {
				try {
					writer.endArray();
				} catch (JSONException e2) {
					System.out.println("Exception while migrating FAQ");
				}

				String test = null;

				faqQA = null;
				StringBuffer sb = sw.getBuffer();
				faqQA = sb.toString();
				templateFile = "sdcfaq.xml";
			} if(isNavigation){
				
				templateFile = "navigation.xml";
				
			}else {
			
				
				templateFile = "sdcinternal.xml";
			}

			try {
				docDir = pkg.createFolder(null, "English", "sdcinternal.xml");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			File doc = null;

			String pageTitle = valuesMap.get("title");

			if (pageTitle.startsWith("\n")) {
				try {
					errorWriter
							.write("\n [ERROR} This page doesn't have a valid title");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			pageTitle = Jsoup.parse(pageTitle).text();
			String jcrName = JcrUtil.createValidName(pageTitle,
					JcrUtil.HYPHEN_LABEL_CHAR_MAPPING);
			String pageName = fileName.substring(fileName.lastIndexOf(File.separator),fileName.length() - 4);
			if (fileName.endsWith("index.xml")) {
				doc = docDir;
			} else {
				doc = new File(docDir, pageName);
				int counter = 1;
				while (doc.exists()) {
					System.out.println("Directory already existed");
					doc = new File(docDir, pageName + "-" + counter++);
				}
				doc.mkdir();
				pkg.addContent(doc.getPath().substring(18));
			}

			File contentFile = new File(doc, ".content.xml");
			try {
				out = new OutputStreamWriter(new FileOutputStream(contentFile),
						"UTF8");
				mappingWriter.write(fileName + ","
						+ doc.getPath().substring(18) + ".html\n");
			} catch (IOException e) {
				System.out.println("Exception file creating file");
			}

			try {
				Template t = new Template(templateFile);

				for (Map.Entry<String, String> entry : valuesMap.entrySet()) {
					if(entry.getKey().equals("title")){
						t.setParameter(entry.getKey(), pageTitle);
					}else{
						t.setParameter(entry.getKey(), entry.getValue());
					}
					
				}

				if (faq) {
					faq = false;
					t.setParameter("faq", faqQA);
				}
				if (isNavigation) {
					isNavigation = false;
					t.setXMLParameter("menuLinks", templteWriter.getBuffer().toString());
					
				}

				t.write(out);
				errorWriter.write("\nMigrating page :"
						+ doc.getPath().substring(18));
				out.close();
				valuesMap.clear();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return;
		}


		if (!element.isEmpty()) {

			
			if (element.equals("Link_URL")) {
				if (tmpValue != null && !tmpValue.startsWith("\n")) {
					String referencePath = tmpValue;
					try {
						String migratedPath = MigrateReferencesUtil.getMigratedPath(referencePath, fileName, errorWriter);
						valuesMap.put("mainLink", migratedPath);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					isSublinks = false;
				}else{
					isSublinks = true;
					valuesMap.put("mainLink", null);
				}
				return;

			}
			
			if (element.equals("dep_sub_image")) {
				if (tmpValue != null && tmpValue.startsWith("/page")) {
					String imagePath = tmpValue.substring(6);
					try {
						String migratedPath = AssetManager.getMigratedAsset(
								imagePath, fileName, errorWriter);
						valuesMap.put("pageImage", migratedPath);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}

			if (migrationMap.get(element) != null) {

				if (isDescStarted) {
					isDescStarted = false;
					String text = textDesc.toString();
					text = migrateAssets(text,
							"\\s*(?i)href\\s*=\\s*(\\\"([^\"]*)|'[^']*'|([^'\">\\s]+))");
					text = migrateAssets(text,
							"\\s*(?i)src\\s*=\\s*(\\\"([^\"]*)|'[^']*'|([^'\">\\s]+))");
					valuesMap.put(migrationMap.get(element), text);
					textDesc.delete(0, textDesc.length());
				} else {
					valuesMap.put(migrationMap.get(element), tmpValue);
				}

			}

			return;
		}

	}

	private String migrateAssets(String text, String regex) {

		List<String> allMatches = new ArrayList<String>();
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(text);
		while (matcher.find()) {
			allMatches.add(matcher.group(1));
			String assetPath = matcher.group(2);
			if (assetPath != null && !assetPath.startsWith("/content")) {
				try {

					if ((assetPath.contains(".html")
							|| assetPath.endsWith(".asp")
							|| assetPath.endsWith("/") || assetPath
								.contains("#")) && !assetPath.startsWith("#")) {
						text = MigrateReferencesUtil.getMigratedReferences(
								text, fileName, assetPath, errorWriter);
					} else if (assetPath.contains(".")
							&& !assetPath.contains("#")
							&& !assetPath.endsWith("/")) {
						String migratedPath = AssetManager.getMigratedAsset(
								assetPath, fileName, errorWriter);
						if (migratedPath != null) {
							text = text.replaceAll("\"" + assetPath + "\"",
									"\"" + migratedPath + "\"");
						}
						errorWriter.write("\n [INFO] References in page :"
								+ matcher.group(1));
					}

				} catch (Exception e) {
					// System.out.print("******Exception while migrating references*********");
				}
			}

		}

		return text;
	}

	@Override
	public void characters(char[] ac, int i, int j) throws SAXException {

		if (isDescStarted) {
			textDesc.append(new String(ac, i, j));
			return;
		}

		tmpValue = new String(ac, i, j);

	}

	public void run() {

		progressBar.setDisable(false);
		progressBar.setProgress(0);

		File migrationFolder = new File("migration");
		if (migrationFolder.exists()) {
			System.out.println("Migration folder already exsists :"
					+ migrationFolder.getAbsolutePath());
			try {
				FileUtils.deleteDirectory(migrationFolder);
			} catch (IOException e) {
				System.out
						.println("Exception while deleting the migration directory");
			}
		}

		errofile = new File("." + File.separator + "error.log");
		// if file doesn't exists, then create it

		try {
			if (!errofile.exists()) {
				errofile.createNewFile();
			}
			FileWriter fw = new FileWriter(errofile.getAbsoluteFile());
			errorWriter = new BufferedWriter(fw);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		mappingfile = new File("." + File.separator + "mapping.csv");
		// if file doesn't exists, then create it

		try {
			if (!mappingfile.exists()) {
				mappingfile.createNewFile();
			}
			FileWriter fw = new FileWriter(mappingfile.getAbsoluteFile());
			mappingWriter = new BufferedWriter(fw);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		final String CONTENT_DIR = Migrator.getCqDestDir();
		final String sourceDirectory = Migrator.getSourceDir();

		String mapping = Migrator.getConfigurationFile().getAbsolutePath();
		new ContentMigration(mapping);

		if (Migrator.isAddCdata()) {
			System.out.println("Adding CDATA to source files");
			MigrationUtil.addCdata(sourceDirectory, migrationMap);
			System.out.println("CDATA added sucessfully");
		}

		String contentDir = CONTENT_DIR;
		contentDirec = contentDir;
		// Package pkg = new Package(contentDir);

		String contentPackageName = "SDC-content-1.0";

		File folder = new File(sourceDirectory);

		// Parsing through all the neurtal XML files in the source directory
		for (File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				System.out.println("directory name: " + fileEntry.getName());
				migrateDirectory(fileEntry, folder);
			} else {
				fileName = fileEntry.getAbsolutePath().substring(
						Migrator.getSourceDir().length());
				if (fileName.endsWith(".xml")) {
					setCurrentFile(fileName);

					new ContentMigration(fileEntry.getAbsolutePath());
				}
			}

		}
		// Add package metadata and zip the jcr_root and META-INF folders
		try {
			errorWriter.close();
			mappingWriter.close();
			pkg.save(contentPackageName);
		} catch (IOException e) {
			try {
				errorWriter.write("Input Output Exception " + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		System.out.println("Done.");
		progressBar.setProgress(1);
		progressBar.setDisable(true);
		startButton.setDisable(false);

	}

	private void migrateDirectory(File currentFolder, File sourceFolder) {

		boolean check = new File(currentFolder, "index.xml").exists();
		System.out.println("Validation for index: " + check);

		for (File fileEntry : currentFolder.listFiles()) {
			if (fileEntry.isDirectory()) {
				System.out.println("directory name: " + fileEntry.getName());
				migrateDirectory(fileEntry, sourceFolder);
			} else {
				fileName = fileName = fileEntry.getAbsolutePath().substring(
						Migrator.getSourceDir().length());
				if (fileName.endsWith(".xml")) {
					setCurrentFile(fileName);
					new ContentMigration(fileEntry, sourceFolder);
				}
			}

		}

	}

}