package com.adobe.ags.migrator.parser;

import java.io.BufferedWriter;

import org.apache.http.HttpResponse;
import org.apache.commons.validator.UrlValidator;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.adobe.ags.migrator.Migrator;
import com.adobe.ags.migrator.dam.AssetMigration;
 
public class AssetManager {
 
	private final static String USER_AGENT = "Mozilla/5.0";
 
	// HTTP GET request
	public static String getMigratedAsset(String asset, String fileName, BufferedWriter errorWriter) throws Exception {
 
		
		
		String department = Migrator.getCqDestDir();
		department = department.substring(department.lastIndexOf("/"));
		System.out.println("\nAsset name : " + asset);
		String url = null;
		if(!asset.contains(".html") && asset!=null){
			if(asset.startsWith("..")){
				if(asset.contains(department)){
					asset = asset.replace(department, "");
				}
				url = "http://www.sdcounty.ca.gov"+department+asset.substring(2);
			}else if(asset.contains("sdcounty.ca.gov") || asset.contains("co.san-diego.ca.us")){
				url = asset;
			}else if(asset.startsWith("/")){
				 url = "http://www.sdcounty.ca.gov"+asset;
			}else if(!asset.contains("http://")){
					url = "http://www.sdcounty.ca.gov"+department+fileName.substring(0,fileName.lastIndexOf("/"))+"/"+asset;
			}else{
				return null;
			}
			
			UrlValidator urlValidator = new UrlValidator();
			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet(url);
	 
			// add request header
			request.addHeader("User-Agent", USER_AGENT);
			HttpResponse response = client.execute(request);
			if(response.getStatusLine().getStatusCode() ==200){
				System.out.println("\nSending 'GET' request to URL : " + url);
				String migratedAsset = AssetMigration.migrateAsset(response, url, errorWriter);
				request.releaseConnection();
				return migratedAsset;
			}else{
				return null;
			}
			
		}else{
			return null;
		}
		
	}
 
}
