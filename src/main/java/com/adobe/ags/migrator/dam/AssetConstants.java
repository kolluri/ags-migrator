package com.adobe.ags.migrator.dam;

public interface AssetConstants {

    /**
     * Renditions folder name
     */
    static final String RENDITIONS_FOLDER = "renditions";

    /**
     * metadata folder name
     */
    static final String METADATA_FOLDER = "metadata";

    /**
     * relations node name
     */
    static final String RELATIONS_NODE = "related";

    /**
     * Asset node type
     */
    static final String NT_ASSET = "dam:Asset";

    /**
     * Asset content node type
     */
    static final String NT_ASSETCONTENT = "dam:AssetContent";

    /**
     * Original file name
     */
    static final String ORIGINAL_FILE = "original";

    /**
     * node types
     */
    static final String NT_SLING_ORDEREDFOLDER = "sling:OrderedFolder";

}