package com.adobe.ags.migrator.dam;

import org.apache.jackrabbit.JcrConstants;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.jackrabbit.util.Text;
import org.apache.sling.jcr.resource.JcrResourceUtil;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.io.InputStream;

public class AssetManager implements AssetConstants, JcrConstants {

    private final Session session;

    public AssetManager(Session session) {
        this.session = session;
    }


    public Node createAsset(final String path, final InputStream is, final String mimeType, final boolean doSave) {
        Node asset = null;

        try {
            String fileName = Text.escapeIllegalJcrChars(path.substring(path.lastIndexOf("/")+1,path.length()));
            System.out.println(fileName);
            Node dam_node = session.getRootNode().getNode("content").getNode("dam");
            Node test_node = JcrUtils.getOrAddNode(dam_node,"test", NT_SLING_ORDEREDFOLDER);
            if (session.hasPendingChanges()) {
                session.save();
            }

            Node assetNode = JcrUtils.getOrAddNode(test_node, fileName, NT_ASSET);
            final Node content = assetNode.addNode(JCR_CONTENT, NT_ASSETCONTENT);
            content.setProperty("cq:name",fileName); //optional - cq specific
            content.addNode(METADATA_FOLDER, NT_UNSTRUCTURED);
            Node renditions = content.addNode(RENDITIONS_FOLDER, NT_FOLDER);
            content.addNode(RELATIONS_NODE, NT_UNSTRUCTURED);
            JcrUtils.putFile(renditions,"original",mimeType,is);
            if (session.hasPendingChanges()) {
                session.save();
            }
        } catch (RepositoryException re) {
            System.out.println(re.getMessage());
        }

        return asset;
    }

    public Node getAsset(String assetPath) {
        Node asset = null;

        try {
            asset = session.getNode(assetPath);
        } catch (Exception e) {
            //ignored
        }

        if (asset != null) {
            return asset;
        }
        return null;
    }

    public boolean assetExists(String assetPath) {
        return getAsset(assetPath) != null;
    }

    public void removeAsset(String assetPath) throws Exception{
        try {
            session.removeItem(assetPath);
        } catch (RepositoryException e) {
            throw new Exception("Failed to remove Asset [ "+ assetPath +" ]", e);
        }
    }

}
