package com.adobe.ags.migrator.dam;

import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;

import javax.jcr.Session;
import java.io.File;
import java.io.FileInputStream;

public class DamRunnable implements Runnable {

    File sourceDirectory = null;
    Session session = null;
    ProgressBar progressBar = null;
    Button startButton = null;

    public DamRunnable(File directory, Session session, ProgressBar pb, Button startButton) {
        sourceDirectory = directory;
        this.session = session;
        progressBar = pb;
        this.startButton = startButton;
    }

    public void run() {
        if (sourceDirectory.isDirectory()) {
            File files[] = sourceDirectory.listFiles();

            AssetManager manager = new AssetManager(session);

            progressBar.setDisable(false);
            progressBar.setProgress(0);
            try {
                for (int i = 0; i < files.length; i++) {
                    if (isDigitalAsset(files[i])) {
                        System.out.println(files[i].getAbsolutePath());
                        manager.createAsset("/content/dam/test/" + files[i].getName(), new FileInputStream(files[i]), "image/jpeg", false);
                        progressBar.setProgress(i+1/files.length);
                    }
                }
                System.out.println("done.");
                progressBar.setProgress(1);
                progressBar.setDisable(true);
                startButton.setDisable(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //System.out.println(sourceDirectory.getAbsolutePath());
    }

    public boolean isDigitalAsset(File fsObj) {
        boolean rval = false;

        if (fsObj.getName().toLowerCase().endsWith("jpg")) {
            rval = true;
        }
        return rval;
    }
}
