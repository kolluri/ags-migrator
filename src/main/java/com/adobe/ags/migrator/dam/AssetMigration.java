package com.adobe.ags.migrator.dam;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.http.HttpResponse;

import com.adobe.ags.migrator.Migrator;

public class AssetMigration {

	
	public static String migrateAsset(HttpResponse response, String url, BufferedWriter errorWriter){
		
		String serverUri = getServerUri();
		String damPath = serverUri.substring(7) + "/content/dam/sdc";
		String cqFilePath = null;
		if(url.contains("sdcounty.ca.gov")){
			 cqFilePath = url.replace("sdcounty.ca.gov", damPath);
			 
		}
		if(url.contains("co.san-diego.ca.us")){
			 cqFilePath = url.replace("co.san-diego.ca.us", damPath);
		}
		cqFilePath = cqFilePath.replace("www.", "");
		
	
		
		System.out.println("CQ file path :"+cqFilePath);
		String folderPath = cqFilePath.
			    substring(0,cqFilePath.lastIndexOf("/"));
		
		InputStreamRequestEntity inre;
		String migratedPath = null;
		try {
			inre = new InputStreamRequestEntity(response.getEntity().getContent());
			URI uri = new URI(cqFilePath, false);
			HttpClient cqclient = createWebdavConnection();
			
			
			if(Migrator.isMigrateAssets() && checkResource(cqclient,uri)){
				createFolder(folderPath, cqclient);
				PutMethod put = new PutMethod(uri.toString());
				System.out.println("Migrating asset :"+uri.toString());
				put.setRequestEntity(inre);
				cqclient.executeMethod(put);
				put.releaseConnection();
			}
			migratedPath = cqFilePath.substring(serverUri.length());

		} catch (IllegalStateException e) {
			try {
				errorWriter.write("\n [TIMEOUT - ERROR] while migrating  "+url);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.out.println("Exception while performing asset migration (illegal state)" + e.getMessage());
		} catch (IOException e) {
			try {
				errorWriter.write("\n [TIMEOUT - ERROR] while migrating  "+url);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.out.println("Exception while performing asset migration " + e.getMessage());
		}
		
		return migratedPath;
	}
	
	
	private static boolean checkResource(HttpClient cqclient, URI uri) throws HttpException, IOException {
		
		GetMethod get = new GetMethod(uri.toString());
		if(cqclient.executeMethod(get) == 200){
			System.out.println("resource already migrated");
			return false;
		}
		get.releaseConnection();
		
		return true;
	}


	/**
	 * This method takes migration configuration as input and setup a Webdav
	 * connection with CQ server
	 * 
	 * @param migrationProperties
	 * @return HttpClient
	 */
	private static HttpClient createWebdavConnection() {
		String serverUri = getServerUri();
		String webdavuri = serverUri + "/crx/repository/crx.default";
		HostConfiguration hostConfig = new HostConfiguration();
		hostConfig.setHost(webdavuri);
		HttpConnectionManager connectionManager = new MultiThreadedHttpConnectionManager();
		HttpConnectionManagerParams params = new HttpConnectionManagerParams();
		params.setMaxConnectionsPerHost(hostConfig,
				MultiThreadedHttpConnectionManager.DEFAULT_MAX_HOST_CONNECTIONS);
		int timeout = Integer.parseInt(Migrator.getMigrationTimeout())*1000;
		params.setSoTimeout(timeout);
		connectionManager.setParams(params);
		HttpClient client = new HttpClient(connectionManager);
		Credentials creds = new UsernamePasswordCredentials(Migrator.getUser(), Migrator.getPass());
		client.getState().setCredentials(AuthScope.ANY, creds);
		client.setHostConfiguration(hostConfig);
		return client;
	}
	
	/**
	 * 
	 * @param migrationProperties
	 * @return
	 */
	private static String getServerUri() {
		String serverUri = Migrator.getServer();
		return serverUri;
	}
	
	
	/**
	 * This method checks for the availability of folder in CQ, if it is not
	 * present it will create a folder
	 * 
	 * @param path
	 * @param client
	 */
	private static void createFolder(String path, HttpClient client) {
		GetMethod get = new GetMethod(path);
		try {
			client.executeMethod(get);
			get.releaseConnection();
			if (get.getStatusCode() == 404) {
				PostMethod set = new PostMethod(path);
				client.executeMethod(set);
				set.releaseConnection();
			}
		} catch (HttpException e) {
			System.out.println("HTTP Exception while creating folder: " + path);
		} catch (IOException e) {
			System.out.println("IO Exception while creating folder: " + path);
		}
	}
}
